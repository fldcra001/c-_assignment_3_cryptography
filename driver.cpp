/*
 * CSC3022H - Assignment 3
 * Cryptography
 * Driver
 * 
 * Craig Feldman
 * 18 April 2014
 * Finalised: 8 May 2014
 */

 #include "cmdline_parser.h"
 #include "cryptomachine.h"
 #include "catch.hpp"

#define CATCH_CONFIG_RUNNER

 #include <fstream>
 #include <iostream>
 cmdline_parser parser;

 
 int main(int argc, char* argv[])
 {
	 using namespace std;
	 using namespace fldcra001;

	 
	//------------------------------------------------------ parser
	

	if(!parser.process_cmdline(argc, argv))
	{
		std::cerr << "Couldn't process command line arguments" << std::endl;
		return 1;
	}

	if(parser.should_print_help()) {
		parser.print_help(std::cout);	
		return 0;
	}
		
	bool encode = parser.should_encode();
	bool decode = parser.should_decode();
	bool group = parser.should_group();
	bool pack = parser.should_pack();
	
	string inputFile = parser.get_inputFilename();
	string outputFile = parser.get_outputFilename();
	string mode = parser.get_mode();
	string xKey = parser.get_xKey();
	string vKey = parser.get_vKey();
	string iv	= parser.get_iv();
	
	bool isXOR = vKey == "null"; //true if xor, false if vignere
	
	//------------------------------------------------------ parser
	
	
	//print out the selected settings
	cout << "Here are the selected options:\n" << endl;	
	cout << "Input file: '" << inputFile << "'\t\tOutput file: '" << outputFile << "'\n" << endl;
	cout <<  "encode\t" << "decode\t" << "group\tpack\tcipher\t\tkey";
	
	if (mode == "cbc") 
		cout << "\tIV";
		
	cout << "\n" << encode << "\t" << decode << "\t" << group << "\t" << pack;	
	
	if (isXOR)
		cout << "\tXOR\t\t" << xKey << "\t";
	else
		cout << "\tVigenere\t" << vKey << "\t";	
		
	if (mode == "cbc") 
		cout << iv;
	cout << endl;
		
	
	std::ofstream fout(outputFile, std::ofstream::binary);
	std::ifstream fin(inputFile, std::ifstream::binary);
	
	//======= XOR =========
	
		//====ECB=====
	
			//========ENCODE=========
			
			//No group;  No pack
			if (isXOR && mode == "ecb" && encode && !group && !pack) {
				cryptomachine<XOR, ecb, 0, 0> c;
				c.setData(xKey);					//sets the key
				c.encode(fin, fout);
			}
			
			//No group; pack
			if (isXOR && mode == "ecb" && encode && !group && pack) {
				cryptomachine<XOR, ecb, 0, 1> c;
				c.setData(xKey);					
				c.encode(fin, fout);
			}

			
			//XOR; ECB; Encode; group; No pack
			if (isXOR && mode == "ecb" && encode && group && !pack) {
				cryptomachine<XOR, ecb, 1, 0> c;
				c.setData(xKey);					
				c.encode(fin, fout);
			}
			
			//XOR; ECB; Encode; group; pack
			if (isXOR && mode == "ecb" && encode && group && pack) {
				cryptomachine<XOR, ecb, 1, 1> c;
				c.setData(xKey);					//sets the key
				c.encode(fin, fout);
			}
			
			//========DECODE=========
			
			//XOR; ECB; Decode; No Pack
			if (isXOR && mode == "ecb" && decode && !pack) {
				cryptomachine<XOR, ecb, 0, 0> c;
				c.setData(xKey);
				c.decode(fin, fout);
			}
			
			//XOR; ECB; Decode; Pack
			if (isXOR && mode == "ecb" && decode && pack) {
				cryptomachine<XOR, ecb, 0, 1> c;
				c.setData(xKey);
				c.decode(fin, fout);
			}
			
			
		//=====CBC=====		
		
			//========ENCODE=========
			
			// No group; No pack
			if (isXOR && mode == "cbc" && encode && !group && !pack) {
				cryptomachine<XOR, cbc, 0, 0> c;
				c.setData(xKey, iv);					//sets the key and initiliasation vector
				c.encode(fin, fout);
			}
			
			// group; No pack
			if (isXOR && mode == "cbc" && encode && group && !pack) {
				cryptomachine<XOR, cbc, 1, 0> c;
				c.setData(xKey, iv);					
				c.encode(fin, fout);
			}
			
			// group; pack
			if (isXOR && mode == "cbc" && encode && group && pack) {
				cryptomachine<XOR, cbc, 1, 0> c;
				c.setData(xKey, iv);					
				c.encode(fin, fout);
			}
			
			// No group; No pack
			if (isXOR && mode == "cbc" && encode && !group && pack) {
				cryptomachine<XOR, cbc, 0, 0> c;
				c.setData(xKey, iv);					
				c.encode(fin, fout);
			}
			
			
			//========DECODE=========
			
			if (isXOR && mode == "cbc" && decode) {
				cryptomachine<XOR, cbc, 0, 0> c;
				c.setData(xKey, iv);
				c.decode(fin, fout);
			}
			

	
	//======= VIGENERE =========
	
		//=========ENCODE===========
		 
		 // group; no pack
		if (!(isXOR) && mode == "ecb" && encode && group && !pack) {
			cryptomachine<vignere, ecb, 1, 0> c;
			c.setData(vKey);
			c.encode(fin, fout);
		}
		
		// group; pack
		if (!(isXOR) && mode == "ecb" && encode && group && pack) {
			cryptomachine<vignere, ecb, 1, 1> c;
			c.setData(vKey);
			c.encode(fin, fout);
		}
		
		//no group; no pack
		if (!(isXOR) && mode == "ecb" && encode && !group && !pack) {
			cryptomachine<vignere, ecb, 0, 0> c;
			c.setData(vKey);
			c.encode(fin, fout);
		}
		
		//no group; pack
		if (!(isXOR) && mode == "ecb" && encode && !group && pack) {
			cryptomachine<vignere, ecb, 0, 1> c;
			c.setData(vKey);
			c.encode(fin, fout);
		}
	
		//=========DECODE===========
		
		//Vigenere ECB decode
		if (!(isXOR) && mode == "ecb" && decode && !pack) {
			cryptomachine<vignere, ecb, 0, 0> c;
			c.setData(vKey);
			c.decode(fin, fout);
		}
		
		if (!(isXOR) && mode == "ecb" && decode && pack) {
			cryptomachine<vignere, ecb, 0, 1> c;
			c.setData(vKey);
			c.decode(fin, fout);
		}
	
	return 0;
 }
 
 

 
