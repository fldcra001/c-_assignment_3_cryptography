/* Traits for cryptomachine
 * Craig Feldman
 * Finalised: 8 May 2014
 */ 

#ifndef _CRYPTO_TRAITS_H
#define _CRYPTO_TRAITS_H

#include "simple_types.h"


namespace fldcra001 {
	
	

/* general */
template <typename cipher, typename mode> class CryptoTraits
{ static std::string key;};


// Vigenere ECB
template <> class CryptoTraits <vignere, ecb>
{
public:

	typedef std::string keyType;

	const static int keySize = 0; // any size allowed	
	static std::string key;
	static std::string getKey(void) { return key;}
		
};


// XOR ECB
template <> class CryptoTraits <XOR, ecb>
{
public:
	typedef uint32_t keyType;

	const static int keySize = 4; // 4 bytes == 32 bit key
};

//XOR CBC
template <> class CryptoTraits <XOR, cbc>
{
public:
	typedef uint32_t keyType;
	typedef uint32_t IVkeyType;		//initialisation vector
	const static int keySize = 4; 	// 4 bytes == 32 bit key
};

}


#endif
