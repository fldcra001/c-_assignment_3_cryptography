**The original assignment requirements and instructions can be found in the 'Downloads' section.**

Templated cryptographic class that can encrypt plaintext messages and decrypt ciphertext.

*********************
Author: Craig Feldman
Date: 	05 May 2014
*********************

=======================
Assignment 3 - CryptoMachine
Readme.txt
========================


---------
  Note
---------

I have provided a file 'TestInput.txt' to be used for the unit tests (which are conducted using a separate testing program to the main program).
I have also provided input.txt which can be used to perform encoding with.

----------------
  Instructions
----------------

1. Compile the application using the 'make' command.

2. Compile the unit test class using 'g++ tests.cpp -o test -std=c++11'

3. Run ./test -s to perform the unit tests.
		
	Note that although its says tests have failed, you can see from the output that the this is not the case and the strings are equal (some known required results were used). I believe this is due to some hidden non printing characters being compared.

4. Run the program using the command line arguments as specified in the assignment.

	e.g..

Vigenere ECB
	./prog -e -v LEMON -m ecb -i input.txt -o output.txt  [-p] [-g]
	./prog -d -v LEMON -m ecb -i output.txt -o decode.txt [-p]

XOR ECB
	./prog -e -x 123456789 -m ecb  -i input.txt -o output.txt  [-p] [-g]
	./prog -d -x 123456789 -m ecb  -i output.txt -o decode.txt [-p (not working when string length > 6)]

XOR CBC
	./prog -e -x 123456789 -I 987654321 -m cbc  -i input.txt -o output.txt  [-p] [-g]
	./prog -d -x 123456789 -I 987654321 -m cbc  -i output.txt -o decode.txt [-p (not working)]

Where '123456789' and '987654321' can be substituted with any (<=) 32 bit integer key
and 'LEMON' can be substituted with any alphabetic key.

The XOR ciphers will show you the steps taken and the XORs performed. 

The encode with pack options do not output the encoded text as the results were not be printable.

[My pack method does successfully work for the XOR cipher, I can see this based on the binary output and file size, however the unpack does not work when the string size to encode is greater than 6. I am aware of what is causing the issue. It occurs due to more than one 32 bit string needing to be XORed and the need to pad the bits to a factor of 32 (I cant think of a way to solve this).]

5. Run make clean to remove class files.



--------------------------
  List of important files
--------------------------

driver.cpp			- processes provided arguments and makes necessary method calls.

cryptomachine.h		- calls required method in policy class

crypto_traits.h 	- traits

crytpo_policy.h		- policies (this is where the main code is)
[Note: has to be in .h file due to templating]

simple_types.h		- some simple classes used for templating
	
tests.cpp			- used for unit testing

input.txt			- some default text to test with
TestInput.txt		- used for unit testing

--------
  Info
--------

I used Ubuntu on a virtual machine for this project. All code has been tested and compiles and runs as expected on the senior lab pcs.
'Geany' was used as the IDE.
 
The makefile generates an executable called 'prog'.

Note that more extensive unit testing has been done by me, more than that shown by the test program.

Also note that when the pack option is selected to encode, one can see that this is implemented correctly by looking at the reduced file size of the output and seeing that it corresponds to 5 bits per character (plus a bit for stuff like new line char).

This program successfully encrypts and decrypts A..Z and the space character as required.

For the XOR ciphers, I tried to show how the encryption/decryption goes about its business by showing the binary operations being performed.

Please note, I main used functors for the encoding/decoding of the XOR ciphers. C++11 Lambdas were used for performing tasks such as converting a key to capitals.
I made use of templated classes which use a traits and a policies class.
STL containers and algorithms have been used extensively, (e.g. iterators and std::transform, std::copy, etc.)
		  
-------------------
  Troubleshooting
-------------------

 Ensure the all files exist in the same directory.
 This program uses c++ 11 features. So please ensure you have the latest version/compiler.

 

 Enjoy!