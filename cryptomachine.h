/* 
 * cryptomachine.h
 * Craig Feldman
 * 18 April 2014 */
 
#ifndef CRYPTOMACHINE_H
#define CRYPTOMACHINE_H 
 
 #include <istream>
 #include <ostream>
 
  #include "crypto_traits.h"
  #include "crypto_policy.h"
  #include "simple_types.h"


namespace fldcra001 {
		
 template<typename Cipher, typename Mode, bool group, bool pack, typename Traits=CryptoTraits<Cipher, Mode>,	typename Policies=CryptoPolicy<Cipher, Mode, group, pack> >
 class cryptomachine {
		 
    public:
		typedef Traits Tr;
		typedef Policies Po;
		
		std::string key;
		std::string iv; 
	 
		//sets key and init vector
		void setData(std::string k, std::string initV)
		{key = k; iv = initV;}
	 
		//sets key
		void setData(std::string k)
		{key = k; iv ="";}

		void encode(std::istream & in, std::ostream & out)
		{	
			// note iv is blank where its not required
			Po::encode(in, out, key, iv);	
		}
		
		void decode(std::istream & in, std::ostream & out)
		{	Po::decode(in, out, key, iv);	}
		
		
 };
 

}
 #endif
