/* Policies for cryptomachine
 * Craig Feldman
 * Finalised: 8 May 2014
 */ 


#ifndef _CRYPTO_TPOLICY_H
#define _CRYPTO_POLICY_H

#include "simple_types.h"
#include "crypto_traits.h"
#include <iostream>
#include <algorithm>
#include <iterator>     // Needed for ostream_iterator
#include <vector>
#include <string>
#include <sstream>
#include <bitset>

namespace fldcra001 {	
	
//-------------------------------------------------------
//------------------ -FUNCTORS- -------------------------

// General form of an encoding functor that performs
// encoding on type T. Does nothing as the default case
template <typename T, typename Encoding> class encoder
{
public:
    T operator()(const T & value) const
        { throw std::exception(); }
};

template <typename T, typename Encoding> class decoder
{
public:
    T operator()(const T & value) const
        { throw std::exception(); }
};

//------------------------------------

//vigener ecb (not used)
template <> class encoder<vignere, ecb>
{
public:
    char operator()(const char value) const
    {
		/*
		 char c = value;
		  if(c >= 'a' && c <= 'z')
			c += 'A' - 'a';
		  else if(c < 'A' || c > 'Z')
			continue;
	 
		  output += (((c - 'A') + (key[j] - 'A')) % 26) + 'A';  // Cipher = (message[i] + key[i]) mod 26 where 'A' = 0
		  j = (j + 1) % key.length(); // wraps key so it matches text length
		  * */
		  return 'A';
	}
};

//------------------------------------

//encoder functor for XOR ecb
template <> class encoder<XOR, ecb> {
	
private: uint32_t key;

public:
		//constructors
	encoder() {};
	encoder(uint32_t k) : key(k){};
	

    std::string operator()(std::string value) 
    {
		using namespace std;

		string binaryString;
		for (size_t i = 0; i < value.size(); ++i)
		{
			bitset<8>b(value[i]);
			binaryString += b.to_string();
		}

		bitset<32>s(binaryString);
		bitset<32>k(key);
		bitset<32>result(s^k);
		
		cout << "\nValue :\t"<< s.to_string() << endl;
		cout <<   "Key   :\t" << k.to_string() << endl;
		cout <<   "XOR   :\t" << result.to_string() << endl;;

		return result.to_string();
	}
};

//DECODER functor for XOR ecb
template <> class decoder<XOR, ecb> {

private: uint32_t key;
public:
		//constructors
	decoder() {};
	decoder(uint32_t k) : key(k){};
	
    std::string operator()(std::string value) 
    {
		
		//std::cout << "START" << std::endl;
		//std::cout << "DECODER: " << value << " " << value.length() << std::endl;

		using namespace std;
		if (value.length() < 32) return "";

		bitset<32>s(value);
		bitset<32>k(key);
		bitset<32>result(s^k);
		
		
		cout << "\nValue:\t" << s.to_string() << endl;
		cout <<   "Key  :\t" << k.to_string() << endl;
		cout <<   "XOR  :\t" << result.to_string() << endl;
		
			
		stringstream sstream(result.to_string());
		string output;
		while(sstream.good() && sstream.str().length() >= 8)
		{
			bitset<8> bits;
			sstream >> bits;
			char c = char(bits.to_ulong());
			output += c;
		}

		return output;		
	}				
};


//encoder functor for XOR CBC
template <> class encoder<XOR, cbc> {
	
private: uint32_t key;
		 uint32_t iv;

public:
		//constructors
	encoder() {};
	encoder(uint32_t k, uint32_t iv32) : key(k), iv(iv32){};

    std::string operator()(std::string value) 
    {
		using namespace std;
		string binaryString;
		//cout<<value<<endl;
			//build the 32 bit binary from string
		for (size_t i = 0; i < value.size(); ++i)
		{
			bitset<8>b(value[i]);
			binaryString += b.to_string();
		}
		
		bitset<32>s(binaryString);
		bitset<32>ivBits(iv);
		bitset<32>k(key);
		bitset<32>result(s^ivBits);	//plaintext XORed with IV
		
		result ^= k;							//xor this value with the key
		iv = (uint32_t)result.to_ulong();		//set iv to new value
		
		cout << "\nValue  :\t" << s.to_string() << endl;
		cout <<   "IV     :\t" << ivBits.to_string() << endl;
		cout <<   "XOR 1  :\t" << result.to_string() << endl;
		cout <<   "Key    :\t" << k.to_string() << endl;
		cout <<   "XOR 2  :\t" << result.to_string() << endl << endl;
		
		return result.to_string();
	}
};


// Decoder functor for XOR CBC
template <> class decoder<XOR, cbc> {
	
private: uint32_t key;
		 uint32_t iv;

public:
		//constructors
	decoder() {};
	decoder(uint32_t k, uint32_t iv32) : key(k), iv(iv32){};

    std::string operator()(std::string value) 
    {
		using namespace std;
		string binaryString;
					
		if (value.length() < 8) return "";

			//build the 32 bit binary from string
		for (size_t i = 0; i < value.size(); ++i)
		{
			bitset<8>b(value[i]);
			binaryString += b.to_string();
		}
		//cout << value << endl;

		bitset<32>s(value);
		bitset<32>ivBits(iv);
		bitset<32>k(key);
		bitset<32>result(s^ivBits);	//plaintext XORed with IV
				

		result ^= k;			//xor this value with the key
		iv = s.to_ulong();		//set iv to new value
		
		cout << "\nValue  :\t" << s.to_string() << endl;
		cout <<   "IV     :\t" << ivBits.to_string() << endl;
		cout <<   "XOR 1  :\t" << result.to_string() << endl;
		cout <<   "Key    :\t" << k.to_string() << endl;
		cout <<   "XOR 2  :\t" << result.to_string() << endl << endl;
		
		stringstream sstream(result.to_string());
		string output;
			//convert back to character
		while(sstream.good() && sstream.str().length() >= 8)
		{
			bitset<8> bits;
			sstream >> bits;
			char c = char(bits.to_ulong());
				output += c;
		}
		
		return output;
	}
};



//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
	

//general base case
//template <typename Cipher, typename Mode> class CryptoPolicy{};
template <typename Cipher, typename Mode, bool group, bool pack> class CryptoPolicy{};



//policy for XOR cipher with cbc mode
template <bool group, bool pack> class CryptoPolicy<XOR, cbc, group, pack>
{
public:
	static void encode(std::istream & in, std::ostream & out, std::string key, std::string iv) {
		using namespace std;
		//cout << "DEBUG: encoding XOR in CBC mode" << endl;
		//cout << iv << endl;;
		
				//convert key to uint32
		uint32_t k32 = atoi(key.c_str());
		uint32_t iv32 = atoi(iv.c_str());
		
		//read file into vector
		std::vector<char> src;
		
		if (group) {
			std::vector<char> tmp((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());
                               
            //group 
            for (int i=0, j=0; i<tmp.size(); ++i) {
				if (tmp[i] != ' ')
				{
					if (j%5 == 0)
						src.push_back(' ');
					src.push_back(tmp[i]);
					++j; //index for src vector
				}
			}	
		}
		else {//not group 
			src = std::vector<char>((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());
		}
	
                               
        std::vector<std::string> srcBlocks;
                               
        std::string block = "";
        std::stringstream ss;
                               
        for(unsigned int i = 0; i < src.size(); ++i) {
			ss << src.at(i);
			if ((i+1) % CryptoTraits<XOR, ecb>::keySize == 0 ){			// new block
				srcBlocks.push_back(ss.str());							//read into blocks of specified bytes
				ss.str(""); //clear
				continue;
			}
			
			if (i == src.size()-1 && (i % CryptoTraits<XOR, ecb>::keySize != 0)){
				srcBlocks.push_back(ss.str());									//shorter than key
			}
		}
		
		std::vector<std::string> dest(src.size()); 

		
		//calls functor and constructs it with the key as an argument to constructor
		std::transform(srcBlocks.begin(), srcBlocks.end(), dest.begin(),
			encoder<XOR, cbc>(k32, iv32));	//functor takes key, init vec
			
		
		 // Print out destination container
		std::cout << "After Encoding:\n";
		std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(std::cout, char()));
		std::cout << std::endl;
		
		std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(out, char()));
		out << std::endl;
		
	}
	
		static void decode(std::istream & in, std::ostream & out, std::string key, std::string siv) {
		//std::cout << "DEBUG: decoding XOR in CBC mode" << std::endl;
		
						
			uint32_t k = atoi(key.c_str());
			uint32_t iv = atoi(siv.c_str());
			
			//read file into vector
			std::vector<char> src((std::istreambuf_iterator<char>(in)),
								   std::istreambuf_iterator<char>());
								   
			std::vector<std::string> srcBlocks;
								   
			std::string block = "";
			std::stringstream ss;
			 
			for(unsigned int i = 0; i < src.size(); ++i) {
				
				ss << src.at(i);
				
				if ((i+1) % (CryptoTraits<XOR, cbc>::keySize *8) == 0 ){		// 32 bit block
					srcBlocks.push_back(ss.str());								//read into blocks of specified bytes
					ss.str(""); //clear
					continue;
				}
				
				if (i == src.size()-1){
					srcBlocks.push_back(ss.str());								//shorter than key
				}
			}
			
			std::vector<std::string> dest(srcBlocks.size()); 
			

			//calls functor and constructs it with the key and initilialisation vector as arguments to constructor
			std::transform(srcBlocks.begin(), srcBlocks.end(), std::back_inserter(dest),
				decoder<XOR, cbc>(k, iv));
				
			
			 // Print out destination container
			std::cout << "After Decoding: ";
			std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(std::cout, char()));
			std::cout << std::endl;
			
			std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(out, char()));
			out << std::endl;
		
	}
	
};



//policy for vignere cipher in ecb mode
template  <bool group, bool pack> class CryptoPolicy<vignere, ecb, group, pack>
{
public:

	// encodes the input using vignere cipher with user specified key
	static void encode(std::istream & in, std::ostream & out, std::string key, std::string iv) {
		//std::cout << "DEBUG: encoding Vignere in ecb mode." << key << std::endl;
		using namespace std;
		
		//convert key to upper (C++11 lambda)
		std::transform(key.begin(),key.end(), key.begin(),[](char c) 
			{return toupper(c); });
		
		std::string  output;
		
		//read file into vector
		std::vector<char> src((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());
		std::vector<char> tmp(src.size());

		
		for(unsigned int i = 0, j = 0; i < src.size(); ++i)
		{
		  char c = src.at(i);
		  if(c >= 'a' && c <= 'z')
			c += 'A' - 'a';
		  else if((c < 'A' || c > 'Z'))
			continue;
	 
		  tmp[i]= (((c - 'A') + (key[j] - 'A')) % 26) + 'A';  	// Cipher = (message[i] + key[i]) mod 26 where 'A' = 0
		  j = (j + 1) % key.length(); 							// wraps key so it matches text length
		}
		
		
		//apply grouping
		std::vector<char> dest;
		
		if (group) {
            //group 
            for (int i=0, j=0; i<tmp.size(); ++i) {
					// groups the ciphertext into groups of 5
				if (isalpha(tmp[i]))
				{
					if (j%5 == 0)
						dest.push_back(' ');
					dest.push_back(tmp[i]);
					++j;
				}
			}
			dest.push_back(' ');

		}
		
		else dest = tmp;
		
	//--------------------------------------------------------------------------------
		// apply packing
		if (pack) {
			string bits;
			//cout << "\nPacking" << endl;
			for (int i=0; i < dest.size() -1; ++i) {	//-1 to not take \n
				bitset<5>s(dest[i]);	//takes 5 least signifcant bits
				//cout << dest[i] << " : " << s.to_string() << endl;

				bits += s.to_string();
				//cout.write(&dest[i] << 3, 1);
			}
			
			int sizeToAdd = 8 - (bits.length() % 8);
			//bitset<10 + sizeToAdd>p(bits);
			vector<bool>packed;
		//	cout << bits << endl;
			//
			bitset<8>s((char)sizeToAdd);
		//	cout << "binary " << s.to_string() << endl;
			for (int i=7; i>=0; --i) 
				packed.push_back(s[i]);
				
				//apply padding
			for (int i=0; i < sizeToAdd; ++i)
				packed.push_back(0);
				
			for (int i=0; i < bits.length(); ++i)
				(bits[i] == '1') ?	packed.push_back(1) : packed.push_back(0);
				
			// build a vector of bytes
			vector<char> bytes;;
			stringstream byteBuilder;
			for (int i=0; i <= packed.size() ; ++i)
			{
				if (byteBuilder.str().size() == 8)
				{
					bitset<8>b(byteBuilder.str());
					bytes.push_back(b.to_ulong());
					if (i == packed.size())	//pushed last byte
						break;
					byteBuilder.str("");	//clear
					byteBuilder << packed[i];

				}
				else {
					byteBuilder << packed[i];
				}
			//	cout << byteBuilder.str() << endl;
			}
			
			//write to file		
			for (int i=0; i < bytes.size(); ++i) {
								out.write((char*)&bytes[i], 1);

			}

		}
	//-------------------------------------------------------------------------------
		
		else {
			//no packing, output ciphertext to file	
			 // Print out destination container
			std::cout << "\nAfter Encoding: ";
			std::copy(dest.begin(), dest.end(), std::ostream_iterator<char>(std::cout, char()));
			std::cout << std::endl;
			
			//write to file
			std::copy(dest.begin(), dest.end(), std::ostream_iterator<char>(out, char()));
			out << std::endl;
		}
		
		cout << "\nFile Encoded" << endl;
		
	}
	
		// decodes the input using vignere cipher with user specified key
	static void decode(std::istream & in, std::ostream & out, std::string key, std::string iv) {	
		
		using namespace std;
		
		//convert key to upper (C++11 lambda)
		std::transform(key.begin(),key.end(), key.begin(),[](char c) 
			{return toupper(c); });
		
		std::string  output;
		
		//read file into vector
		std::vector<char> src((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());

		std::vector<char> dest(src.size());		
		
		string ss = "";	//to hold the bits
		int pad;
		
		for(unsigned int i = 0, j = 0; i < src.size(); ++i)
		{
		  char c = src.at(i);
		  if (!isalpha(c) && !pack) {	//space
			dest.push_back(' ');
			continue;
		  }
		  //std::cout << "'" << c << "'" << std::endl;;

		//---------------------------------------------------------------------------
		  if (pack) {
			  
			  if (i==0) {
				 // ss << src[0];
				  bitset<8>p(src[0]);
				  pad = (int)p.to_ulong();
				 // cout << "pad "<< pad << endl;		
				  for(unsigned int k = 1; k < src.size(); ++k) {
					//  cout << "Unpacking : " << src[k] << endl;
						bitset<8>b(src[k]);			//convert the char to bitset
						//  c = b;
						ss += b.to_string();
					//	cout << "ss " << ss << endl;	
					}
					
					ss = ss.substr(pad, ss.length());
					//cout << "final string " << ss << endl;
					continue;
				  
			   }
			   
			  bitset<8>byte(ss.substr(0, 5));
			 // cout << "substr : " << ss.substr(0, 5) << endl;
			  //cout << "byte " << byte.to_string() << endl;
			  
			  //check if we must output a space
			  if (byte.to_string() == "00000000") {
				dest.push_back(' ');
				//--i;
				ss = ss.substr(5, ss.length());
				if (ss.length() > 0) --i;		//repeat loo;
				continue; 
			  }
			  
			  c = byte.to_ulong() +  '@';		//this unpacks it to the correct 8 bit number (adds 01000000)
			  ss = ss.substr(5, ss.length());
				if (i == src.size() - 1 && ss.length() > 0)	//still have to decode more
					--i;		
		  }
 		//---------------------------------------------------------------------------
 
			c = (c - key[j] + 26) % 26 + 'A';
			if (isalpha(c)) {
				dest.push_back(c); 
				j = (j + 1) % key.length(); // wraps key so it matches text length
			}
		  
		}

		
		 // Print out destination container
		std::cout << "\nAfter Decoding: ";
		std::copy(dest.begin(), dest.end(), std::ostream_iterator<char>(std::cout, char()));
		std::cout << std::endl;
		
		std::copy(dest.begin(), dest.end(), std::ostream_iterator<char>(out, char()));
		out << std::endl;
		
	}

};



//XOR, ECB
template <bool group, bool pack> class CryptoPolicy<XOR, ecb, group, pack>
{
public:
	static void encode(std::istream & in, std::ostream & out, std::string key, std::string iv) {

		using namespace std;
		
		//convert key to uint32
		uint32_t k = atoi(key.c_str());
		
		//read file into vector
		std::vector<char> src;
		
		if (group) {
			std::vector<char> tmp((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());
                               
            //group 
            for (int i=0, j=0; i<tmp.size(); ++i) {
				if (tmp[i] != ' ')
				{
					if (j%5 == 0)
						src.push_back(' ');
					src.push_back(tmp[i]);
					++j; //index for src vector
				}
			}	
		}
		else {//not group 
			src = std::vector<char>((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());
		}
		
		//--------------------------------------------------------------------------------
		vector<char> bytes;
		int sizeToAdd;
		
		// apply packing
		if (pack) {
			string bits;
			//cout << "PACKING" << endl;
			for (int i=0; i < src.size() -1; ++i) {	//-1 to not take \n
				if (!isalpha(src[i]))
					continue;
				bitset<5>s(src[i]);	//takes 5 least signifcant bits

				bits += s.to_string();

			}
			
			if ((bits.length() % 8) != 0)	
				sizeToAdd = 8 - (bits.length() % 8);
			else sizeToAdd = 0;
			
			//bitset<10 + sizeToAdd>p(bits);
			vector<bool>packed;
			//cout << "PAD: " << sizeToAdd << endl;

			//add padding
			for (int i=0; i < sizeToAdd; ++i)
				packed.push_back(0);
				
			for (int i=0; i < bits.length(); ++i)
				(bits[i] == '1') ?	packed.push_back(1) : packed.push_back(0);
				
			//for (bool b : packed)
			//		cout << b;	cout <<endl;
				
			// build a vector of bytes
			stringstream byteBuilder;
			for (int i=0; i <= packed.size() ; ++i)
			{
				if (byteBuilder.str().size() == 8)
				{
									//cout << "Adding byte: " << byteBuilder.str() << endl;

					bitset<8>b(byteBuilder.str());
					bytes.push_back(b.to_ulong());
					if (i == packed.size())	//pushed last byte
						break;
					byteBuilder.str("");	//clear
					byteBuilder << packed[i];

				}
				else {
					byteBuilder << packed[i];
				}
			}
			
		}
	//--------------------------------------------------------------------------------

                           
        std::vector<std::string> srcBlocks;
                               
        std::string block = "";
        std::stringstream ss;
        
        if (pack)
			src = bytes;
                               
        for(unsigned int i = 0; i < src.size(); ++i) {
			ss << src.at(i);
			//cout << "AT I " << src.at(i) << endl;;
			if ((i+1) % CryptoTraits<XOR, ecb>::keySize == 0 ){				// new block
				srcBlocks.push_back(ss.str());								//read into blocks of specified bytes
				cout << "pushing " << ss.str() << endl;
				ss.str(""); //clear
				continue;
			}

		}
			if (ss.str().length() > 0) 
				srcBlocks.push_back(ss.str());									//shorter than key
		
		std::vector<std::string> dest(src.size()); 

		
		//calls functor and constructs it with the key as an argument to constructor
		std::transform(srcBlocks.begin(), srcBlocks.end(), dest.begin(),
			encoder<XOR, ecb>(k));
			
		
		 // Print out destination container
		if (!pack) {
			std::cout << "\nAfter Encoding:\n";
			std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(std::cout, char()));
			std::cout << std::endl;
		
			std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(out, char()));
			out << std::endl;
		}
		
		else {
				//write bytes file					
			for (int i=0; i < dest.size(); ++i) {
				string bin32 = dest[i];
				//cout << "start " << bin32 << endl;
				
				//breaks the 32 bits into 8 bits to be written at a time
				for (int j=0, k=0; k<4 ; j += 8, ++k){
					if (bin32.length() < 32) continue;
					bitset<8>bin8(bin32.substr(j, j+8));
					unsigned long charToWrite = bin8.to_ulong() ;
					out.write(reinterpret_cast<const char*>(&charToWrite), 1);
					//cout << "adding " << bin8 << endl;
				}
			}
			//out << sizeToAdd;
		}
		
		cout << "\nEncoding complete." << endl;
	
	
	}
	
	static void decode(std::istream & in, std::ostream & out, std::string key, std::string iv) {
		
		using namespace std;
		
		uint32_t k = atoi(key.c_str());
		
		//read file into vector
		std::vector<char> src((std::istreambuf_iterator<char>(in)),
                               std::istreambuf_iterator<char>());
                               
        std::vector<std::string> srcBlocks;
                               
        std::string block = "";
        std::vector<std::string> dest(srcBlocks.size()); 

	        stringstream ss;
			for(unsigned int i = 0; i < src.size(); ++i) {

				if (!pack)
					ss << src.at(i);
				else
					ss << bitset<8>(src.at(i));
					
				
				if (ss.str().size() % (CryptoTraits<XOR, ecb>::keySize *8) == 0 ){		// 32 bit block
					srcBlocks.push_back(ss.str());			//read into blocks of specified bytes

					ss.str(""); //clear
					continue;
				}
				
				if (i == src.size()-1){
					//have some stuff left
					srcBlocks.push_back(ss.str());	
				}
			}
 		//---------------------------------------------------------------------------
 		

		//calls functor and constructs it with the key as an argument to constructor
		std::transform(srcBlocks.begin(), srcBlocks.end(), std::back_inserter(dest),
			decoder<XOR, ecb>(k));
			
			
		//---------------------------------------------------------------------------
		if (pack) {
			vector<string> temp;
			string chunk; // joining all the chunks
			string smallChunk; 
			
			//put the bits together
			for(unsigned int i = 0; i < dest.size(); ++i) { 
			//	bitset<32>chunk(dest[i] );
			
				for (int k = 0; k < dest[i].length()-1; ++k) {
					bitset<8>bits(dest[i][k]);
					smallChunk += bits.to_string();
				}
				//chunk = smallChunk;
				chunk = smallChunk + chunk;
				smallChunk = "";
			
			  }
			  

			  		// now we have all the characters as binary, read the characters from the right				
				while (chunk.length() > 5) {
					string bits = chunk.substr(chunk.length() - 5, chunk.length() - 1);
					chunk = chunk.substr(0, chunk.length() - 5);
				//	cout << bits << endl;
					
					bitset<8>byte(bits);
					char c = byte.to_ulong() +  '@';
					if (c == '@') continue;
					//cout << "decoded char: " << c << endl;
					temp.push_back(string(1, c));
				}
				
			  //read from right
			  std::reverse(temp.begin(), temp.end());
			  				dest = temp;

		}
 	//---------------------------------------------------------------------------
			
		
		 // Print out destination container
		std::cout << "\nAfter Decoding: ";
		std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(std::cout, char()));
		std::cout << std::endl;
		
		std::copy(dest.begin(), dest.end(), std::ostream_iterator<std::string>(out, char()));
		out << std::endl;
		
	}
};




}
#endif
