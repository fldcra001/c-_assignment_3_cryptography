# make file
# CSC 3022H Assignment 2
# Craig Feldman

CC = g++													# compiler	
CXXFLAGS=-std=c++0x	-Wall									#  header +all warnings; c++11
LIBS= -L/usr/lib/ -lboost_program_options			# library (boost)
TARGET=prog						# executable name
OBJECTS= driver.o cmdline_parser.o cryptomachine.o			# object files to build into exe

# Linking Rule
$(TARGET): $(OBJECTS) $(HEADERS)
	$(CC) $(OBJECTS) -o $(TARGET) $(LIBS) 
#@cp $(TARGET) ./binaries 				#move to binaries

driver.o: driver.cpp
cmdline_parser.o: cmdline_parser.cpp cmdline_parser.h
cryptomachine.o: cryptomachine.cpp cryptomachine.h crypto_traits.h crypto_policy.h

# Macro to associate *.o with *.cpp
.cpp.o:
	$(CC) $(CXXFLAGS) -c $<

clean:
	@rm -f *.o
