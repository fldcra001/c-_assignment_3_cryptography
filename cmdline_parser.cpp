// Modified by Craig Feldman

#include "cmdline_parser.h"

// Definition of static strings in the class
const std::string cmdline_parser::INPUTFILE     = "input";
const std::string cmdline_parser::OUTPUTFILE    = "output";
const std::string cmdline_parser::NUMBERTHREADS = "threads";

//-------------------------------------------------------------------------//
// Constructor, initialise the variables_map object with default
// constructor, options_descriptor with the name "Options"
//-------------------------------------------------------------------------//
cmdline_parser::cmdline_parser(void) : vm(), od("Options")
{
	// Shorter alias for the boost::program_options namespace
	namespace po = boost::program_options;

	// Add cmdline options
	// --help or -?
	//etc
	od.add_options()
		("help,?", "produce help message")
		
		("encode,e", "encodes input")
		("decode,d", "decodes input")
		("group,g", "Specify support for grouping into code blocks")
		("pack,p", "Specify support for bit packing")
		
		("inputFile,i", po::value<std::string>()->default_value("input.dat"),  "input filename")
		("outputFile,o", po::value<std::string>()->default_value("output.dat"), "output filename")
		
		("xKey,x", po::value<std::string>()->default_value("null"), "use specified key for XOR cipher")
		("vKey,v", po::value<std::string>()->default_value("null"), "use specified key for Vignere cipher")

		("iv,I", po::value<std::string>()->default_value(""), "use provided init. vector with XOR when in CBC mode")
		
		("mode,m", po::value<std::string>()->default_value("CBC"), "use specified mode 'CBC' or 'ECB'");
		


};

//-------------------------------------------------------------------------//
// Process the cmdline
//-------------------------------------------------------------------------//
bool cmdline_parser::process_cmdline(int argc, char * argv[])
{
	// Shorter alias for the boost::program_options namespace
	namespace po = boost::program_options;

	// Clear the variable map
	vm.clear();

	// Parse the cmdline arguments defined by argc and argv,
	// looking for the options defined in the od variable,
	// and store them in the vm variable.
	po::store(po::parse_command_line(argc, argv, od), vm);
	po::notify(vm);

	// Assume processing always succeeds
	return true;
}

//-----------------------------------------------------------------------//
// Return the input filename
//-------------------------------------------------------------------------//
std::string cmdline_parser::get_input_filename(void) const
{
	// Return whatever value is stored as a string
	return vm[INPUTFILE].as<std::string>();
}

//-----------------------------------------------------------------------//
// Return the output filename
//-------------------------------------------------------------------------//
std::string cmdline_parser::get_output_filename(void) const
{
	// Return whatever value is stored  as a string
	return vm[OUTPUTFILE].as<std::string>();
}

//-------------------------------------------------------------------------//
// Get the number of threads to use
//-------------------------------------------------------------------------//
int cmdline_parser::get_number_threads(void)
	{ return vm[NUMBERTHREADS].as<int>(); }

//-----------------------------------------------------------------------//
// Should we print cmdline help?
//-------------------------------------------------------------------------//
bool cmdline_parser::should_print_help(void) const
{
	// Are there instances of the help option stored in the variable map
	return vm.count("help") > 0;
}

//-------------------------------------------------------------------------//
// Print out the options_descriptor to the supplied output stream
//-------------------------------------------------------------------------//
void cmdline_parser::print_help(std::ostream & out) const
{
	out << od;
}

//-------------------------------------------------------------------------//
// Print out the options_descriptor to the supplied output stream
//-------------------------------------------------------------------------//
void cmdline_parser::print_errors(std::ostream & out) const
{
	std::cerr << "Error processing command line arguments:" << std::endl;
	std::copy(errors.begin(), errors.end(),
		std::ostream_iterator<std::string>(out, "\n"));
}


// encode
bool cmdline_parser::should_encode(void) const
{
	// Are there instances of the encode option stored in the variable map
	return vm.count("encode") > 0;
}

// decode
bool cmdline_parser::should_decode(void) const
{
	return vm.count("decode") > 0;
}

// pack
bool cmdline_parser::should_pack(void) const
{
	return vm.count("pack") > 0;
}

// group
bool cmdline_parser::should_group(void) const
{
	return vm.count("group") > 0;
}

//-----------------------------------------------------------------------//
// Return the input filename
//-------------------------------------------------------------------------//
std::string cmdline_parser::get_inputFilename(void) const
{
	//return "test";
	// Return whatever value is stored in input as a string
	return vm["inputFile"].as<std::string>();
}

//-----------------------------------------------------------------------//
// Return the output filename
//-------------------------------------------------------------------------//
std::string cmdline_parser::get_outputFilename(void) const
{
	// Return whatever value is stored in input as a string
	return vm["outputFile"].as<std::string>();
}

//get mode
std::string cmdline_parser::get_mode(void) const
{
	return vm["mode"].as<std::string>();
}

//get xKey
std::string cmdline_parser::get_xKey(void) const
{
	//if (vm.count("xKey") > 0 )
	return vm["xKey"].as<std::string>();
}

//get vKey
std::string cmdline_parser::get_vKey(void) const
{
	return vm["vKey"].as<std::string>();
}

//get iv
std::string cmdline_parser::get_iv(void) const
{
	return vm["iv"].as<std::string>();
}



