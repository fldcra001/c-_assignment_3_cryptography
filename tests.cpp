/* 
 * Performs Unit Testing on cryptomachine class
 * Craig Feldman
 * 8 May 2014
 */


#include "cryptomachine.h"
#define CATCH_CONFIG_MAIN 
#include "catch.hpp"
#include <fstream>
#include <iostream>
 
 	std::ofstream fout("TestOutput.txt", std::ofstream::binary);
	std::ifstream fin("TestInput.txt", std::ifstream::binary);
	std::ofstream decoded("TestDecode.txt", std::ofstream::binary);
	std::ifstream idecoded("TestDecode.txt", std::ifstream::binary);
	std::ifstream encoded("TestOutput.txt", std::ifstream::binary);

	using namespace fldcra001;
	using namespace std;

TEST_CASE("EncodeDecode"){
	SECTION("Testing Vigenere encode") {
		
		cryptomachine<vignere, ecb, 0, 0> c;
		c.setData("LEMON");					//sets the key
		c.encode(fin, fout);
		
		//std::ifstream encoded("TestOutput.txt");
		
		string cipherText;
		getline(encoded, cipherText);
		//cipherText =  	LXFOPVEFRNHR"
		cipherText = cipherText.substr(0, cipherText.length() -1); //remove new line char
		
		REQUIRE(cipherText == "LXFOPVEFRNHR ");
	}	
	
	SECTION("Testing Vigenere decode") {
		cryptomachine<vignere, ecb, 0, 0> c;
		c.setData("LEMON");		
		
		// this stuff is to reset the files
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
			
		c.decode(encoded, decoded);		
		string decodedText;
		getline(idecoded, decodedText);
		
		decodedText.erase(decodedText.find_last_not_of(" \n\r\t")+1);
		
		REQUIRE(decodedText == "ATTACKATDAWN");
	}
	
	SECTION("Testing XOR ECB") {
		
		std::ofstream fout("TestOutput.txt", std::ofstream::binary | ofstream::trunc);
		std::ofstream decoded("Testdecoded.txt", std::ofstream::binary | ofstream::trunc);

		
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
		cryptomachine<XOR, ecb, 0, 0> c;
		c.setData("123456789");					//sets the key
		c.encode(fin, fout);
		
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
		c.decode(encoded, decoded);
		
		string decodedText;
		getline(idecoded, decodedText);		
		decodedText.erase(decodedText.find_last_not_of(" \n\r\t")+1);
		//decodedText = decodedText.substr(10);
		REQUIRE(decodedText == "ATTACKATDAWN");
	}
	
		SECTION("Testing XOR CBC") {
		
		std::ofstream fout("TestOutput.txt", std::ofstream::binary | ofstream::trunc);
		std::ofstream decoded("Testdecoded.txt", std::ofstream::binary | ofstream::trunc);

		
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
		cryptomachine<XOR, cbc, 0, 0> c;
		c.setData("123456789", "987654321");					//sets the key
		c.encode(fin, fout);
		
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
		c.decode(encoded, decoded);
		
		string decodedText;
		getline(idecoded, decodedText);		
		decodedText.erase(decodedText.find_last_not_of(" \n\r\t")+1);
		//decodedText = decodedText.substr(10);
		REQUIRE(decodedText == "ATTACKATDAWN");
	}
	  
}

TEST_CASE("Group"){ 
		SECTION("Testing Vigenere group") {
			INFO ("The output file 'TestOutput.txt must have characters grouped in 5s");

			
		std::ofstream fout("TestOutput.txt", std::ofstream::binary | ofstream::trunc);
		std::ofstream decoded("Testdecoded.txt", std::ofstream::binary | ofstream::trunc);
			
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
		cryptomachine<vignere, ecb, 1, 0> c;
		c.setData("LEMON");					//sets the key
		c.encode(fin, fout);
		
		//std::ifstream encoded("TestOutput.txt");
		
		string cipherText;
		getline(encoded, cipherText);
		//cipherText =  	LXFOPVEFRNHR "
		cipherText = cipherText.substr(1, cipherText.length()); //remove new line char
		
		REQUIRE(cipherText == "LXFOP VEFRN HR ");
	}	
}

TEST_CASE("Pack"){ 
		SECTION("Testing pack on vigenere") {
			INFO ("The output file 'TestOutput.txt must have a smaller size than 'TestInput.txt' and the program must encode/decode correctly");
			
		std::ofstream fout("TestOutput.txt", std::ofstream::binary | ofstream::trunc);
		std::ofstream decoded("Testdecoded.txt", std::ofstream::binary | ofstream::trunc);
			
		encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
		cryptomachine<vignere, ecb, 0, 1> c;
		c.setData("LEMON");					//sets the key
		c.encode(fin, fout);
		
encoded.clear();
		encoded.seekg(0, ios::beg);		
		fout.clear();
		decoded.clear();
		idecoded.clear();
		idecoded.seekg(0, ios::beg);
		fin.clear();
		fin.seekg(0, ios::beg);
		
			
		c.decode(encoded, decoded);		
		string decodedText;
		getline(idecoded, decodedText);
		
		decodedText.erase(decodedText.find_last_not_of(" \n\r\t")+1);
		
		REQUIRE(decodedText == "ATTACKATDAWN");
	}	
}

	
